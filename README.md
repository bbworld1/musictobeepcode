# MusicToBeepCode: Convert music formats to beep codes.

Have you ever wanted to make music with your motherboard?
If you have, this is the right place for you.

This program converts common music formats (MusicXML etc.)
to a script that makes your motherboard beep.
It's as simple as installing this script!*

# Installation:

1. Install prerequisites (`sudo apt install beep` or equivalent)
2. Clone this repository.
3. Install pip dependencies (`pip install --user -r requirements.txt`)
4. Run the Python script (e.g. `python3 musictobeep.py bach.mxl`)

This script works with all file formats that Music21 supports (MusicXML etc.)

# Known Bugs:
- `beep` sometimes won't work without root privileges. Execute the script with `sudo` to avoid.
- `sudo` sometimes complains that the argument list is too long. Don't put `sudo` inside the script. Execute it using `sudo bash beep.sh`.
- Chords are APPROXIMATED. If your song doesn't sound exactly the way you remember it, that's because we dropped a chord somewhere and now it sounds a bit weird. We're still working on improving this, so stay tuned!
