"""
MusicToBeep script
converts mxl to beep
"""
import music21
import argparse

# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument("file")
args = parser.parse_args()

# Open script for writing
f = open("beep.sh", "w")
f.write("beep ")

# Parse score
score = music21.converter.parse(args.file)


for part in (p for p in score if type(p) == music21.stream.Part or
             type(p) == music21.stream.PartStaff):

    # Add notes to script measure by measure
    for measure in (m for m in part if type(m) == music21.stream.Measure):
        try:
            timesig = measure.timeSignature.denominator
        except AttributeError:
            pass

        try:
            notelen = measure.seconds / timesig # Get length of quarter note
        except:
            # Fallback on default (80bpm)
            notelen = 0.75

        for note in (n for n in measure
                     if type(n) == music21.note.Note or
                     type(n) == music21.chord.Chord or
                     type(n) == music21.note.Rest):

            spannerSites = [g for g in note.getSpannerSites()
                            if type(g) == music21.spanner.Glissando]

            if len(spannerSites) != 0:
                # Hack in some of that nice glissando
                # Gotta hit em with that careless whisper

                gliss = spannerSites[0]
                print(gliss)

                start, end = gliss.getSpannedElements()

                if note != end:

                    glissrange = end.pitch.midi - start.pitch.midi
                    glisslen = end.offset - start.offset
                    glisslen -= end.quarterLength
                    if glisslen < 0:
                        glisslen = timesig + glisslen

                    print(glisslen, glissrange)
                    glissnotelen = glisslen * notelen / glissrange
                    endnotelen = end.quarterLength * notelen
                    for n in range(start.pitch.midi, end.pitch.midi):
                        toWrite = music21.note.Note()
                        toWrite.pitch.midi = n
                        print("gotta yeet that glis with ", toWrite)
                        f.write("-n -f {} -l {} ".format(str(toWrite.pitch.frequency),
                                                         str(glissnotelen*1000)))
                    f.write("-n -f {} -l {} ".format(str(end.pitch.frequency),
                                                     str(endnotelen*1000)))

            else:
                # Get note info
                duration = note.quarterLength * notelen
                if type(note) == music21.note.Note:
                    # It's a note
                    f.write("-n -f {} -l {} ".format(str(note.pitch.frequency),
                                                    str(duration*1000)))
                elif type(note) == music21.chord.Chord:
                    # Flatten chord
                    f.write("-n -f {} -l {} ".format(str(note.root().frequency),
                                                     str(duration*1000)))
                else:
                    # It's a rest
                    f.write("-D {} ".format(duration*1000))
    break
